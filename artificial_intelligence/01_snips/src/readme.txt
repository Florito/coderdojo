how to run this example


Follow the tutorial here
https://docs.snips.ai/getting-started/quick-start-macos

-----------------------------------------------------------
Install:
node.js, NPM, Homebrew

Install snips components: 
brew tap snipsco/homebrew-snips

Install snips platform components: 
brew install snips-asr snips-hotword snips-dialogue snips-watch \
    snips-audio-server snips-nlu snips-tts

Install mosquitto client
brew install mosquitto

Setup your assistant
unpack assistant_proj_ZavaXnGBBnN.zip
into /usr/local/share/snips/assistant/
-----------------------------------------------------------
Start:
brew services start mosquitto
brew services start snips-audio-server
brew services start snips-hotword
brew services start snips-tts
brew services start snips-nlu
brew services start snips-asr
brew services start snips-dialogue

Stop:
brew services stop snips-audio-server
brew services stop snips-hotword
brew services stop snips-tts
brew services stop snips-nlu
brew services stop snips-asr
brew services stop snips-dialogue
brew services stop mosquitto


Run the
listenToJson processing app

Or run the node app (install mqtt first)
npm install mqtt --save
node index.js

