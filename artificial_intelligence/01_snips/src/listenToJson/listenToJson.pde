import mqtt.*;

MQTTClient client;

void setup() {
  size(400, 400);
  client = new MQTTClient(this);
  client.connect("mqtt://localhost:1883");//, "hermes/#");
  //client.subscribe("hermes/asr/#");
  client.subscribe("hermes/intent/#");
  //client.subscribe("hermes/intent/#");//asr/#");
  background(0);
  stroke(255);
  strokeWeight(3);
}

void draw() {
}


void messageReceived(String topic, byte[] payload) {
  String msg = new String(payload);
  println("new message: " + topic + " - " + msg);

  //println(topic);
  //println(msg);
  //if (topic.equals("hermes/asr/textCaptured")) {
  //  println(msg);
  JSONObject json = JSONObject.parse(msg);
  //json.parse(msg);
  //  println(json.format(3));

 //println(json.toString());
  JSONObject intent = json.getJSONObject("intent");

  //println(intent);
  
  if (intent!=null) {
    
    float conf = intent.getFloat("confidenceScore");
    String name = intent.getString("intentName");
    
    
    if (conf>0.7) {
      
      println(name);
      if (name.equals("MarcusGraf:triangle")) {
        stroke(255,128);
        noFill();
        triangle(random(width),random(height),random(width),random(height),random(width),random(height));
      }
      if (name.equals("MarcusGraf:square")) {
        stroke(255,128);
        noFill();
        rectMode(CORNERS);
        rect(random(width),random(height),random(width),random(height));
      }
      if (name.equals("MarcusGraf:circle")) {
        stroke(255,128);
        noFill();
        ellipseMode(CORNERS);
       // float r = random(width/4);
        ellipse(random(width),random(height),random(width),random(height));
        try
        { 
            // Just one line and you are done !  
            // We have given a command to start cmd 
            // /K : Carries out command specified by string 
           Runtime.getRuntime().exec(new String[] {"say", "circle"}); 
  
        } 
        catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
      }
      if (name.equals("MarcusGraf:clearscreen")) {
        background(0);
        try
        { 
            // Just one line and you are done !  
            // We have given a command to start cmd 
            // /K : Carries out command specified by string 
           Runtime.getRuntime().exec(new String[] {"say", "fuck you. Hey Snips! Hey Snips! Hey Snips! Hey Snips! Hey snips! delete delete delete."}); 
  
        } 
        catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
      }
      
      
    }
    
  }
  
  
  ///}
}

//void keyPressed() {
// client.publish("/Saskia", "Hey hier is Marcus");
//}
