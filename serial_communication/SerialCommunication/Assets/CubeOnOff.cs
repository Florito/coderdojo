﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System;

public class CubeOnOff : MonoBehaviour {

    public Material onMaterial;
    public Material offMaterial;

    SerialPort port;

    // Use this for initialization
    void Start () {
        List<string> portNames = new List<string>();
        portNames.AddRange(System.IO.Ports.SerialPort.GetPortNames());
        portNames.AddRange(System.IO.Directory.GetFiles("/dev/", "cu.*"));
        String[] ports = portNames.ToArray();
        //Debug.Log(portNames.Count + "available ports: \n" + string.Join("\r\n", portNames.ToArray()));

        Debug.Log("Serial ports:");
        foreach (string str in ports)
        {
            Debug.Log(str);
        }
        Debug.Log("done");

        port = new SerialPort("/dev/cu.usbmodem1432", 115200); // Microbit
        port.ReadTimeout = 50;
        port.Open();

    }

    // Update is called once per frame
    void Update () {
        //Debug.Log("Reading...");
        string incomingMessage = ReadFromArduino();
        if (incomingMessage != null && incomingMessage.Length > 0)
        {
            Debug.Log("Incoming message " + incomingMessage+" ("+incomingMessage.Length+")");
            if (incomingMessage.Contains("left"))
            {
                MoveLeft();
            }
            else if (incomingMessage.Contains("right"))
            {
                MoveRight();
            }
        }
	}

    public void SetOn()
    {
        GetComponent<Renderer>().material = onMaterial;
        WriteToSerial("on");
    }

    public void SetOff()
    {
        GetComponent<Renderer>().material = offMaterial;
        WriteToSerial("off");
    }

    public void MoveLeft() {
        Vector3 newPos = transform.position + new Vector3(-0.2f, 0, 0);
        transform.position = newPos;
    }

    public void MoveRight() {
        Vector3 newPos = transform.position + new Vector3(0.2f, 0, 0);
        transform.position = newPos;
    }




    public string ReadFromArduino(int timeout = 20)
    {
        port.ReadTimeout = timeout;
        try
        {
            return port.ReadLine();
        }
        catch (TimeoutException e)
        {
            return null;
        }
    }

    public void WriteToSerial(string message)
    {
        Debug.Log("Writing " + message + " to serial...");
        port.WriteLine(message + "\n"); // serial read line in MicroBit wants this
        port.BaseStream.Flush();
    }

    /*
     * MICROBIT SCRIPT:
     * 
let incomingMessage = ""
serial.onDataReceived(serial.delimiters(Delimiters.NewLine), function () {
    incomingMessage = serial.readUntil(serial.delimiters(Delimiters.NewLine))
    if (incomingMessage.compare("on") == 0) {
        led.plot(2, 2)
    } else if (incomingMessage.compare("off") == 0) {
        led.unplot(2, 2)
    }
    basic.showString(incomingMessage)
})
input.onButtonPressed(Button.A, function () {
    serial.writeLine("left")
})
input.onButtonPressed(Button.B, function () {
    serial.writeLine("right")
})
basic.showString("OK")
*/



    /** 
     * @see https://www.alanzucconi.com/2015/10/07/how-to-integrate-arduino-with-unity/
     */


    /*
StartCoroutine
(
    AsynchronousReadFromArduino
    ((string s) => Debug.Log(s),     // Callback
        () => Debug.LogError("Error!"), // Error callback
        10000f                          // Timeout (milliseconds)
    )
);
*/

    /*
    public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity)
    {
        DateTime initialTime = DateTime.Now;
        DateTime nowTime;
        TimeSpan diff = default(TimeSpan);

        string dataString = null;

        do
        {
            try
            {
                dataString = port.ReadLine();
            }
            catch (TimeoutException)
            {
                dataString = null;
            }

            if (dataString != null)
            {
                callback(dataString);
                yield break; // Terminates the Coroutine
            }
            else
                yield return null; // Wait for next frame

            nowTime = DateTime.Now;
            diff = nowTime - initialTime;

        } while (diff.Milliseconds < timeout);

        if (fail != null)
            fail();
        yield return null;
    }
    */

}
