﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class SerialComm : MonoBehaviour {

    SerialPort port;
	// Use this for initialization
	void Start () {

        // check available ports:
        Debug.Log("Serial ports:");
        foreach (string str in SerialPort.GetPortNames()) {
            Debug.Log(str);
        }
        Debug.Log("done");

        //port = new SerialPort("COM4", 9600);
        //port.ReadTimeout = 50;
        //port.Open();

        //WriteToSerial("hi");
    }

    public void WriteToSerial(string message)
    {
        Debug.Log("Writing " + message + " to serial...");
        port.WriteLine(message+".");
        port.BaseStream.Flush();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
