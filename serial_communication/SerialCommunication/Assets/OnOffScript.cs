﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class OnOffScript : MonoBehaviour {

    public Material brightMaterial;
    public Material darkMaterial;

    //string portName = "COM4"; // windows
    string portName = "/dev/cu.usbmodem14112"; // OSX

    SerialPort port;

    // Use this for initialization
    void Start () {
        List<string> portNames = new List<string>();
        portNames.AddRange(System.IO.Ports.SerialPort.GetPortNames());
        portNames.AddRange(System.IO.Directory.GetFiles("/dev/", "cu.*"));
        string[] ports = portNames.ToArray();
        //Debug.Log(portNames.Count + "available ports: \n" + string.Join("\r\n", portNames.ToArray()));

        Debug.Log("Serial ports:");
        foreach (string str in ports)
        {
            Debug.Log(str);
        }
        Debug.Log("done");

        port = new SerialPort(portName, 115200); // Microbit
        port.ReadTimeout = 50;
        port.Open();
    }



    public string ReadFromArduino(int timeout = 20)
    {
        port.ReadTimeout = timeout;
        try
        {
            return port.ReadLine();
        }
        catch (System.TimeoutException e)
        {
            return null;
        }
    }

    // Update is called once per frame
    void Update()
    {

        string message = ReadFromArduino();
        if (message != null)
        {
            Debug.Log(message.Trim());

            if (message.Trim().Equals("left")) {
                MoveLeft();
            }
            if (message.Trim().Equals("right"))
            {
                MoveRight();
            }
        }
    }



    public void MakeBright() {
        GetComponent<Renderer>().material = brightMaterial;

        port.WriteLine("on"); // serial read line in MicroBit wants this
        port.BaseStream.Flush();
    }


    public void MakeDark()
    {
        GetComponent<Renderer>().material = darkMaterial;

        port.WriteLine("off"); // serial read line in MicroBit wants this
        port.BaseStream.Flush();
    }


    public void MoveRight() {
        transform.position = transform.position + new Vector3(0.5f, 0, 0);
    }
    public void MoveLeft()
    {
        transform.position = transform.position + new Vector3(-0.5f, 0, 0);
    }






}
