import mqtt.*;

MQTTClient client;

void setup() {
 size(400,400);
 client = new MQTTClient(this);
 client.connect("mqtt://0d533cad:3914dc8a670a83be@broker.shiftr.io", "Marcus");
 //client.connect("mqtt://try:try@broker.shiftr.io", "Marcus");
 client.subscribe("/Drawing");
 // client.unsubscribe("/example");
 background(0);
 stroke(255);
 strokeWeight(3);
}

void draw() {
}

void mouseDragged() {
 client.publish("/Drawing", "dr "+mouseX+" "+mouseY);
}

void messageReceived(String topic, byte[] payload) {
 String msg = new String(payload);
 println("new message: " + topic + " - " + msg);
 if (msg.indexOf("dr")==0) {
   String[] params = split(msg, " ");
   if (params.length==3) {
     int x = parseInt(params[1]);
     int y = parseInt(params[2]);
     point(x,y);
   }
 }
}

void keyPressed() {
 client.publish("/Saskia", "Hey hier is Marcus");
}
