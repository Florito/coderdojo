import mqtt.*;
import java.util.*;

MQTTClient client;

void setup() {
  size(400, 400);
  setupClient();
  
  // client.unsubscribe("/example");
  background(0);
  stroke(255);
  strokeWeight(3);
}

void setupClient() {
  println("Setting up client on "+new Date());
  client = new MQTTClient(this);
  client.connect("mqtt://try:try@broker.shiftr.io", "Marcus");
  client.subscribe("/Drawing");
}

void draw() {
}

void mouseDragged() {
  try {
    if (client!=null) {
      client.publish("/Drawing", "dr "+mouseX+" "+mouseY);
    }
  } 
  catch (Exception e) {
    //println(e);
    //client.disconnect();
    //client.dispose();
    client = null;
    setupClient();
  }
}

void messageReceived(String topic, byte[] payload) {
  String msg = new String(payload);
  //println("new message: " + topic + " - " + msg);
  if (msg.indexOf("dr")==0) {
    String[] params = split(msg, " ");
    if (params.length==3) {
      int x = parseInt(params[1]);
      int y = parseInt(params[2]);
      point(x, y);
    }
  }
}

void keyPressed() {
  client.publish("/Saskia", "Hey hier is Marcus");
}
