﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Rigidbody rb;
    public GameObject floor;
    public GameObject roof;

    public float forwardMove = 20;
    public float sideForce = 500;
    public float jumpForce = 2000;

    bool dead = false;
    bool onFloor = false;
    bool requestJump = false;
    bool inJump = false;

    // Use this for initialization
    void Start () {
		
	}


    private void FixedUpdate()
    {
        if (!dead) {
            //rb.AddTorque(forwardForce * Time.deltaTime, 0, 0);
            float z = rb.position.z;
            rb.MovePosition(new Vector3(rb.position.x, rb.position.y, z+ forwardMove * Time.deltaTime));
                //rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            // steering
            if (Input.GetKey("left"))
            {
                //rb.AddTorque(0, sideForce * Time.deltaTime, 0);
                rb.AddForce(-sideForce * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey("right"))
            {
                rb.AddForce(sideForce * Time.deltaTime, 0, 0);
            }


            if (Input.GetKeyDown("space")) {
                rb.AddForce(0, jumpForce * Time.deltaTime, 0);
            }
            /*
            if ( (Input.GetKeyDown("space") || requestJump) && !inJump)
            {
                if (onFloor)
                {
                    rb.AddForce(0, jumpForce * Time.deltaTime, 0);
                    requestJump = false;
                    inJump = true;
                } else {
                    requestJump = true;
                }

            }
            */
        }

        else {
            //transform.localScale.Scale()
            //transform.localScale = transform.localScale * 0.99f;
        }


    }



    void OnCollisionEnter(Collision col) {
        if (col.gameObject == floor.gameObject) {
            onFloor = true;
            inJump = false;
        }

        if (col.gameObject != floor.gameObject && col.gameObject != roof.gameObject)
        {
            //Die();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject==floor.gameObject) {
            onFloor = false;
        }
    }


    void Die()
    {
        Debug.Log("You died!");
        dead = true;
    }



}
