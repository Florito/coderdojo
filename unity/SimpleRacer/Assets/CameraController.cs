﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject target;
    float distanceToObject;

	// Use this for initialization
	void Start () {
        distanceToObject = target.transform.position.z - transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
        //transform.LookAt(target.transform);
        //transform.Rotate(-15, 0, 0);

        float camZ = target.transform.position.z - distanceToObject;
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            camZ);

    }

}
