﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour {

    public GameObject obstaclePrefab;
    public GameObject roof;
    public int amount = 50;

	// Use this for initialization
	void Start () {
        Bounds roofBounds = roof.GetComponent<BoxCollider>().bounds;

        float x0 = roofBounds.min.x;
        float x1 = roofBounds.max.x;
        float z0 = roofBounds.min.z;
        float z1 = roofBounds.max.z;
        float y = roofBounds.max.y - 1.5f;

        for (int i = 0; i < amount; i++)
        {
            GameObject obstacle = Instantiate(obstaclePrefab);
            obstacle.transform.position = new Vector3(
                Random.Range(x0, x1),
                Random.Range(1,y),
                Random.Range(z0,z1)
            );
            obstacle.transform.Rotate(0, Random.value * 360, 0);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
