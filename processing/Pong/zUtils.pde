PVector[] makeNormalizedPolygon(int sides) {
  PVector[] out = new PVector[sides];
  for (int i=0; i<sides; i++) {
    float rd = map(i, 0, sides, 0, TWO_PI);
    out[i] = new PVector(cos(rd), sin(rd));
  }
  return out;
}

PVector[] deepClone(PVector[] vectors) {
  PVector[] out = new PVector[vectors.length];
  for (int i=0; i<out.length; i++)
    out[i] = vectors[i].copy();
  return out;
}

PVector[] PVectorMultAll(PVector[] vectors, float factor) {
  PVector[] out = new PVector[vectors.length];
  for (int i=0; i<out.length; i++)
    out[i] = PVector.mult(vectors[i], factor);
  return out;
}

PVector[] PVectorAddAll(PVector[] vectors, PVector add) {
  PVector[] out = new PVector[vectors.length];
  for (int i=0; i<out.length; i++)
    out[i] = PVector.add(vectors[i], add);
  return out;
}


/**
 * @returns top/left, right/bottom, width/height
 */
PVector[] getBoundingBox(PVector[] polygon) {
  PVector min = new PVector(Float.MAX_VALUE, Float.MAX_VALUE);
  PVector max = new PVector(Float.MIN_VALUE, Float.MIN_VALUE); 
  for (PVector p : polygon) {
    min.x = min(min.x, p.x);
    max.x = max(max.x, p.x);
    min.y = min(min.y, p.y);
    max.y = max(max.y, p.y);
  }
  return new PVector[] {min, max, PVector.sub(max, min) };
}



PVector getAverage(PVector... points) {
  PVector out = new PVector();
  for (PVector p : points)
    out.add(p);
  out.div(points.length);
  return out;
}




// segment circle collision
// https://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
/*
 E is the starting point of the ray,
 L is the end point of the ray,
 C is the center of sphere you're testing against
 r is the radius of that sphere
 Compute:
 d = L - E ( Direction vector of ray, from start to end )
 f = E - C ( Vector from center sphere to ray start )
 */

boolean isCircleIntersectingWithLine(PVector segmentPoint0, PVector segmentPoint1, PVector centerOfCircle, float radius) {
  
  PVector E = segmentPoint0;
  PVector L = segmentPoint1;
  PVector C = centerOfCircle;
  float r = radius;
  PVector d = PVector.sub(L,E);
  PVector f = PVector.sub(E,C);
  
  float a = PVector.dot(d,d);// d.Dot( d ) ;
  float b = 2*PVector.dot(f,d);//2*f.Dot( d ) ;
  float c = PVector.dot(f,f) - r*r;//f.Dot( f ) - r*r ;

  float discriminant = b*b-4*a*c;
  if ( discriminant < 0 )
  {
    // no intersection
    return false;
  } else
  {
    // ray didn't totally miss sphere,
    // so there is a solution to
    // the equation.

    discriminant = sqrt( discriminant );

    // either solution may be on or off the ray so need to test both
    // t1 is always the smaller value, because BOTH discriminant and
    // a are nonnegative.
    float t1 = (-b - discriminant)/(2*a);
    float t2 = (-b + discriminant)/(2*a);

    // 3x HIT cases:
    //          -o->             --|-->  |            |  --|->
    // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

    // 3x MISS cases:
    //       ->  o                     o ->              | -> |
    // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

    if ( t1 >= 0 && t1 <= 1 )
    {
      // t1 is the intersection, and it's closer than t2
      // (since t1 uses -b - discriminant)
      // Impale, Poke
      return true ;
    }

    // here t1 didn't intersect so we are either started
    // inside the sphere or completely past it
    if ( t2 >= 0 && t2 <= 1 )
    {
      // ExitWound
      return true ;
    }

    // no intn: FallShort, Past, CompletelyInside
    return false ;
  }
}

/* @see https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector */
// r = d - 2(d . n)n, where d . n is dot product and n must be normalized
// 
PVector reflect(PVector d, PVector normal) {
  return PVector.sub( d, PVector.mult(normal, 2*PVector.dot(d,normal)) );
}
