class Paddle {

  final PVector pos0;
  final PVector pos1;
  final PVector fullRange;

  final PVector centerPos0;
  final PVector centerPos1;
  final PVector moveRange;

  final float rotation;
  final PVector orientation;
  final PVector normal;

  final float thickness;
  final float length;

  final Controls controls;

  float positionOnLine = 0.5; // in percentage
  PVector centerPos;

  PVector[] outlinePoints;
  PVector[] outlineNormals;
  boolean[] debugIntersecting;

  Paddle(PVector pos0, PVector pos1, float thickness, float length, Controls controls) {
    this.pos0=pos0;
    this.pos1=pos1;
    this.thickness = thickness;
    this.length = length;
    this.controls = controls;

    // calc fullRange
    fullRange = PVector.sub(pos1, pos0);

    // calc moveRange, orientation and movePositions
    float fullRangeLength = fullRange.mag();  
    if (fullRangeLength<length) {
      throw new RuntimeException("Length of paddle is longer than full range!");
    }

    // calc rotation
    rotation = atan2(fullRange.y, fullRange.x);

    // calc orientation
    orientation = fullRange.copy().normalize();

    // calc normal
    normal = fullRange.copy();
    normal.rotate(HALF_PI);
    normal.normalize();


    // |------XXXX------|
    // | half |

    float lengthPercentage = length/fullRangeLength; // relation length (X) to full range
    //float residuePercentage = 1-lengthPercentage;    // relation residue (-) to full range
    //float halfResiduePerc = residuePercentage/2;
    float toCenterOfPaddlePerc = lengthPercentage/2;
    //println("lengthPercentage: "+lengthPercentage);
    //println("halfResiduePerc: "+halfResiduePerc);

    centerPos0 = PVector.add(pos0, PVector.mult(fullRange, toCenterOfPaddlePerc) );
    centerPos1 = PVector.sub(pos1, PVector.mult(fullRange, toCenterOfPaddlePerc) );
    moveRange = PVector.sub(centerPos1, centerPos0);

    outlineNormals = new PVector[] {
      normal, orientation, PVector.mult(normal, -1), PVector.mult(orientation, -1)
    };
  }




  void tick(Ball ball, float seconds) {


    if (controls.leftDown) positionOnLine-=paddleSpeedInPercentagePerSecond*seconds;
    if (controls.rightDown) positionOnLine+=paddleSpeedInPercentagePerSecond*seconds;

    positionOnLine = constrain(positionOnLine, 0, 1);
    centerPos = PVector.add(centerPos0, PVector.mult(moveRange, positionOnLine));

    PVector paddleLengthVecHalf = PVector.mult( orientation, length*0.5);
    PVector paddleThicknessVecHalf = PVector.mult( normal, thickness*0.5);

    // create outline points
    outlinePoints = new PVector[] {
      PVector.sub(PVector.sub( centerPos, paddleLengthVecHalf ), paddleThicknessVecHalf), 
      PVector.sub(PVector.add( centerPos, paddleLengthVecHalf ), paddleThicknessVecHalf), 
      PVector.add(PVector.add( centerPos, paddleLengthVecHalf ), paddleThicknessVecHalf), 
      PVector.add(PVector.sub( centerPos, paddleLengthVecHalf ), paddleThicknessVecHalf)
    };
    debugIntersecting = new boolean[4];

    // check future intersection:
    for (int i=0; i<4; i++) {
      int i1 = (i+1)%4;
      debugIntersecting[i] = isCircleIntersectingWithLine(outlinePoints[i], outlinePoints[i1], ball.position, ball.radius);
      if (debugIntersecting[i]) {
        //TODO: bounce!
        //ball.movement.set(0, 0);
        
        // bounce without residue:
        ball.movement = reflect(ball.movement,outlineNormals[i]);
        
      }
    }
  }


  void drawRange() {
    strokeWeight(1);
    stroke(128);
    line(pos0.x, pos0.y, pos1.x, pos1.y);
    strokeWeight(3);
    stroke(128);
    line(centerPos0.x, centerPos0.y, centerPos1.x, centerPos1.y);
  }


  void drawNormal() {
    pushMatrix();
    PVector centerPos = PVector.add(centerPos0, PVector.mult(moveRange, positionOnLine));
    translate(centerPos.x, centerPos.y, 0.0);
    PVector normalLine = PVector.mult(normal, 20);
    strokeWeight(1);
    noFill();
    stroke(128);
    line(0, 0, normalLine.x, normalLine.y);
    popMatrix();
  }

  void drawOutline() {
    // outlines
    strokeWeight(3);
    stroke(255, 0, 0);
    noFill();
    beginShape();
    for (int i=0; i<4; i++) {
      PVector p = outlinePoints[i];
      if (debugIntersecting[i]) { 
        stroke(255, 0, 0);
        strokeWeight(3);
      } else { 
        strokeWeight(1);
        stroke(64, 0, 0);
      }
      vertex(p.x, p.y, 3);
    }
    endShape(CLOSE);

    // outlintPoints text
    //fill(255, 128, 128);
    //textAlign(CENTER, CENTER);
    //for (int i=0; i<outlinePoints.length; i++) {
    //  pushMatrix();
    //  translate(outlinePoints[i].x, outlinePoints[i].y, 4);
    //  scale(0.8);
    //  text("p"+i, 0, 0);
    //  popMatrix();
    //}

    // outline normals
    strokeWeight(1);
    stroke(100, 255, 100);
    for (int i=0; i<outlinePoints.length; i++) {
      int i1 = (i+1)%4;
      PVector av = getAverage(outlinePoints[i], outlinePoints[i1]);
      PVector norm = PVector.mult(outlineNormals[i], 40);
      PVector p1 = PVector.add(av, norm);
      line(av.x, av.y, p1.x, p1.y);
    }
  }



  void draw() {

    //TODO:
    strokeWeight(1);
    noFill();
    noStroke();//stroke(255, 200, 200);
    fill(250, 200, 200);

    pushMatrix();

    translate(centerPos.x, centerPos.y, 1.0);
    rectMode(CENTER);
    rotate(rotation);
    rect(0, 0, length, thickness);
    popMatrix();
  }
}
