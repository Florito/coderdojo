// player and playfield settings

int playerAmount = 3;
float goalPadding = 20;
float playerPadding = 40;
float paddleThickness = 10;
float paddleLength = 100;

char[][] controlKeys = new char[][] {
  new char[] { 's', 'a' }, 
  new char[] { 'g', 't' }, 
  new char[] { 'k', 'l' }
};
//char[][] controlKeys = new char[][] {
//  new char[] { 's', 'a' }, 
//  new char[] { 'f', 'd' }, 
//  new char[] { 'g', 't' }, 
//  new char[] { 'h', 'j' }, 
//  new char[] { 'k', 'l' }
//};

float ballSize = 10;
float paddleSpeedInPercentagePerSecond = 1.5;

float ballSpeed = 20;
