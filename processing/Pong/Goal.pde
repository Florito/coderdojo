class Goal {
  
  final PVector pos0;
  final PVector pos1;
  final PVector halfPos;
  final PVector fullRange;
  final float orientation;
  final PVector nameOffset;
  
  Goal(PVector pos0, PVector pos1) {
    this.pos0=pos0;
    this.pos1=pos1;
    halfPos = PVector.div( PVector.add(pos0,pos1), 2.0 );
    fullRange = PVector.sub(pos1,pos0);
    orientation = atan2(fullRange.y, fullRange.x);
    PVector normal = fullRange.copy();
    normal.rotate(HALF_PI);
    normal.normalize();
    nameOffset = PVector.mult(normal,-5);
  }
  
  void drawName(String name) { 
    boolean turnText = orientation<-HALF_PI || orientation>HALF_PI;
    textFont(avenir);
    fill(255);
    textAlign(CENTER,turnText?TOP:BOTTOM);
    noStroke();
    pushMatrix();
    translate(halfPos.x, halfPos.y);
    translate(nameOffset.x, nameOffset.y);
    rotate(orientation + (turnText?PI:0));
    text(name,0,0);
    popMatrix();
  }
  
  void draw() {
    strokeWeight(1);
    stroke(128);
    line(pos0.x, pos0.y, pos1.x, pos1.y);
  }
  
}
