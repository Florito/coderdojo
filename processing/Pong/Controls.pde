class Controls {
  
  final char left, right;
  boolean leftDown, rightDown;
  
  Controls(char left, char right) {
    this.left = left;
    this.right = right;
  }
  
  void keyPressed() {
    if (key==left) leftDown = true;
    if (key==right) rightDown = true;
  }
  void keyReleased() {
    if (key==left) leftDown = false;
    if (key==right) rightDown = false;
  }
  
}
