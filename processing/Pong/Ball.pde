class Ball {
  
  final float radius;
  private PVector position = new PVector(0,0);
  //private PVector futurePosition = new PVector(0,0);
  private PVector movement = new PVector(0,0); // per second
  
  Ball(float radius) {
    this.radius = radius;
  }
  
  void setPosition(PVector pos) {
    position.set(pos);
  }
  
  void setMovement(PVector mov) {
    movement.set(mov);
  }
  
  void tick(float seconds) {
    PVector delta = PVector.mult(movement,seconds);
    position.add( delta );
    //futurePosition = PVector.add(position, delta);
  }
  
  void draw() {
    noStroke();
    fill(255);
    ellipseMode(RADIUS);
    pushMatrix();
    translate(position.x, position.y);
    ellipse(0,0,radius,radius);
    popMatrix();
  }
  
  
}
