class Player {
  
  final String name;
  final Paddle paddle;
  final Goal goal;
  int score;
  
  Player (String name, Paddle paddle, Goal goal) {
    this.name = name;
    this.paddle = paddle;
    this.goal = goal;
  }
  
  void tick(Ball ball, float seconds) {
    paddle.tick(ball, seconds);
  }
  
  void drawPaddle() {
    //paddle.drawRange();
    //paddle.drawNormal();
    //paddle.drawOutline();
    paddle.draw();
    goal.draw();
    goal.drawName(name);
  }
  
}
