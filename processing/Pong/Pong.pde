Player[] players;
Ball ball;
PVector centerOfField;
String player0Name;

PFont avenir;

int lastMS;

void setup() {
  size(800, 800, P3D);
  
  //frameRate(5);
  player0Name = new File(System.getProperty("user.dir")).getName().toUpperCase();
  

  avenir = loadFont("AvenirNext-DemiBold-15.vlw");
  
  createPlayers();
  ball = new Ball(ballSize);
  
  
  
}

void createPlayers() {

  
  
  // make a normalized polygon
  PVector[] polygon = makeNormalizedPolygon(playerAmount);
  PVector[] boundingBox = getBoundingBox(polygon);
  
  // make polygon that fits goalRadius
  float scaleUpGoal = min( (width-2*goalPadding)/boundingBox[2].x, (height-2*goalPadding)/boundingBox[2].y );
  PVector[] goalPoints = PVectorMultAll( polygon, scaleUpGoal );
  
  // make polygon that fits playerRadius
  float scaleUpPlayer = min( (width-2*playerPadding)/boundingBox[2].x, (height-2*playerPadding)/boundingBox[2].y );
  PVector[] playerPoints = PVectorMultAll( polygon, scaleUpPlayer );
  
  // move goalPoints to center make equal padding
  boundingBox = getBoundingBox(goalPoints);
  PVector moveGoalToCenter = PVector.add( 
    new PVector(-boundingBox[0].x, -boundingBox[0].y),
    new PVector((width-boundingBox[2].x)/2, (height-boundingBox[2].y)/2));
  goalPoints = PVectorAddAll(goalPoints, moveGoalToCenter);
  // move playerPoints
  playerPoints = PVectorAddAll(playerPoints, moveGoalToCenter);
  
  centerOfField = getAverage(playerPoints);

  players = new Player[playerAmount];
  for (int i=0; i<playerAmount; i++) {
    PVector pos0 = playerPoints[i];
    PVector pos1 = playerPoints[(i+1)%playerAmount];
    PVector goalPos0 = goalPoints[i];
    PVector goalPos1 = goalPoints[(i+1)%playerAmount];

    
    players[i] = new Player( 
      (i==0 ? player0Name : "Player "+i), 
      new Paddle(pos0, pos1, paddleThickness, paddleLength, new Controls(controlKeys[i][0], controlKeys[i][1])), 
      new Goal(goalPos0, goalPos1)
      );
  }
}


/*
*
*    NEW ROUND
*
*/



void newRound() {
  
  
  ball.setMovement( PVector.mult(PVector.random2D(),ballSpeed) );
  ball.setPosition(centerOfField);
  lastMS = millis();
}



/*
*
 *    
 *
 */


void draw() {
  if (frameCount==2) newRound();
  tickAll();
  drawAll();
}



/*
*
 *    KEYS
 *
 */

void keyPressed() {
  if (key=='R') newRound();
  for (Player player:players) {
    player.paddle.controls.keyPressed();
  }
}
void keyReleased() {
  for (Player player:players) {
    player.paddle.controls.keyReleased();
  }
}


/*
*
 *    TICK
 *
 */


void tickAll() {
  int now = millis();
  float frameTime = (now-lastMS)/1000.0;
  lastMS = now;

  for (Player player : players) {
    
    ball.tick(frameTime);
    player.tick(ball, frameTime);
    
  }
}



/*
*
 *    DRAW
 *
 */



void drawAll() {
  background(0);

  ortho();

  for (Player player : players) {
    player.drawPaddle();
  }
  ball.draw();
  
}
