int initSerial(String[] availablePorts) {
  if (availablePorts == null) {
    println("ERROR: No serial ports available!");
    exit();
  }
  String[] serialConfig = loadStrings(serialConfigFile);
  println("Available ports:");
  println(serialConfig);
  if (serialConfig != null && serialConfig.length > 0) {
    String savedPort = serialConfig[0];
    // Check if saved port is in available ports.
    for (int i = 0; i < availablePorts.length; ++i) {
      if (availablePorts[i].equals(savedPort)) {
        //selectedPort = i;
        return i;
      }
    }
  }
  return 0;
}





// Set serial port to desired value.
void setSerialPort(String portName) {
  // Close the port if it's currently open.
  if (port != null) {
    port.stop();
  }
  try {
    // Open port.
    port = new Serial(this, portName, 115200);
    port.bufferUntil('\n');
    // Persist port in configuration.
    saveStrings(serialConfigFile, new String[] { portName });
  }
  catch (RuntimeException ex) {
    // Swallow error if port can't be opened, keep port closed.
    port = null;
  }
}






void serialEvent(Serial p) 
{
  String incoming = p.readString().trim();
  String[] splits = incoming.split(" ");
  if (printSerial) {
    println(incoming);
  }

  //calibration system 0 gyro 3 accel 0 mag 0
  if (parseCalibrationData(splits)) {
    calLabel.setText("Calibration: Sys="+calSys+" Gyro="+calGyro+" Accel="+calAcc+" Mag="+calMag);
  }

  PVector dataPoint;
  
  dataPoint = getEventData3d(splits, "vector_accelerometer");
  if (dataPoint!=null)
    vector_accelerometerList.add(dataPoint);
   dataPoint = getEventData3d(splits, "vector_linear_accelerometer");
  if (dataPoint!=null)
    vector_linear_accelerometerList.add(dataPoint);
}


/*
Messages being sent:
  calibration system 0 gyro 3 accel 0 mag 0
  temperature 23
  eventtype 1 vector_accelerometer x -0.04 y -0.43 z 9.32
  eventtype 1 vector_linear_accelerometer x 0.02 y 0.04 z -0.46
  eventtype 1 vector_gravity x -0.03 y -0.46 z 9.79
  eventtype 2 vector_magnetometer x 4.87 y -7.06 z -67.06
  eventtype 3 vector_euler x 359.94 y -0.19 z 2.69
  eventtype 11 vector_gyroscope x -0.06 y 0.06 z -0.06
*/
