import processing.serial.*;
import java.awt.datatransfer.*;
import java.awt.Toolkit;
import processing.opengl.*;
import g4p_controls.*;
import java.util.*;

// Serial port state.
Serial       port;
final String serialConfigFile = "serialconfig.txt";
boolean      printSerial = false;

boolean fakeSerial = true;

int calSys, calGyro, calAcc, calMag;

float x0, x1;
float y0, y1;

ArrayList<PVector> vector_accelerometerList = new ArrayList<PVector>();
ArrayList<PVector> vector_linear_accelerometerList = new ArrayList<PVector>();

DataDisplayer vector_accelerometerDisplay;
DataDisplayer vector_linear_accelerometerDisplay;


void setup()
{

  size(800, 480, P3D);
  text("go", 1, 1);

  float tab = 10;
  x0=tab;
  x1 = width-tab;
  y0 =100;
  y1 = height-tab;
  frameRate(30);

  vector_accelerometerDisplay = new DataDisplayer("vecAcc", x1-x0, 70, -20, 20);
  vector_accelerometerDisplay.setPos(x0, y0);
  
  vector_linear_accelerometerDisplay = new DataDisplayer("vecLinAcc", x1-x0, 70, -20, 20);
  vector_linear_accelerometerDisplay.setPos(x0, y0+80);
  
  

  // Serial port setup.
  // Grab list of serial ports and choose one that was persisted earlier or default to the first port.
  String[] availablePorts = Serial.list();
  int selectedPort = initSerial(availablePorts);

  buildUI(availablePorts, selectedPort);
}






void draw()
{

  if (fakeSerial) {
    PVector point = new PVector(
      map(millis()%10000, 0, 10000, -20, 20), 
      sin(map(millis(), 0, 400, 0, TWO_PI))*5, 
      noise(millis()/1000.0)*40-20);
    vector_accelerometerList.add(point);
    point = new PVector(
      map(millis()%8000, 0, 8000, -10, 15), 
      sin(map(millis(), 0, 180, 0, TWO_PI))*5, 
      noise(millis()/3000.0)*40-20);
    vector_linear_accelerometerList.add(point);
  }





  // draw RECT
  background(0, 0, 0);
  noFill();
  stroke(255);
  strokeWeight(1);
  rectMode(CORNERS);
  rect(x0, y0, x1, y1);



  // draw DATA
  vector_accelerometerDisplay.drawComplete(vector_accelerometerList);
  vector_linear_accelerometerDisplay.drawComplete(vector_linear_accelerometerList);
  
  
}
