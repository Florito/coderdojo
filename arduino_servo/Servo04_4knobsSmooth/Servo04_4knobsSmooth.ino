
#include <Servo.h>

Servo myservo0;  // create servo object to control a servo
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
Servo myservo3;  // create servo object to control a servo

#define pot0 0
#define pot1 1
#define pot2 2
#define pot3 3

#define servo0 9
#define servo1 10
#define servo2 11
#define servo3 12

//#define SMOOTHING true
//#define SMOOTH 0.98

#define MEDIAN true
#define MEDIAN_COUNT 20

int v0, v1, v2, v3;

float val0;
float val1;
float val2;
float val3;

int vm0[MEDIAN_COUNT];
int vm1[MEDIAN_COUNT];
int vm2[MEDIAN_COUNT];
int vm3[MEDIAN_COUNT];
int medianIndex = 0;
int med_array[MEDIAN_COUNT];

void setup() {
  myservo0.attach(servo0);
  myservo1.attach(servo1);
  myservo2.attach(servo2);
  myservo3.attach(servo3);
  Serial.begin(115200);
}

void loop() {

  v0 = analogRead(pot0);
  v1 = analogRead(pot1);
  v2 = analogRead(pot2);
  v3 = analogRead(pot3);

  if (MEDIAN) {
    vm0[medianIndex] = v0;
    vm1[medianIndex] = v1;
    vm2[medianIndex] = v2;
    vm3[medianIndex] = v3;
    medianIndex++;
    medianIndex%=MEDIAN_COUNT;
  }

  //  if (SMOOTHING) {
  //    float invSmooth = 1-SMOOTH;
  //    val0 = val0*SMOOTH + v0*invSmooth;
  //    val1 = val1*SMOOTH + v1*invSmooth;
  //    val2 = val2*SMOOTH + v2*invSmooth;
  //    val3 = val3*SMOOTH + v3*invSmooth;
  //  } else {

  if (MEDIAN) {
    int lt_length = sizeof(med_array) / sizeof(med_array[0]);
    
    memcpy(med_array, vm0, sizeof(med_array[0])*MEDIAN_COUNT);
    qsort(med_array, lt_length, sizeof(med_array[0]), sort_desc);
    val0 = med_array[MEDIAN_COUNT/2];

    
    memcpy(med_array, vm1, sizeof(med_array[0])*MEDIAN_COUNT);
    qsort(med_array, lt_length, sizeof(med_array[0]), sort_desc);
    val1 = med_array[MEDIAN_COUNT/2];
    
//    for (int i=0;i<MEDIAN_COUNT;i++) {
//      Serial.print(med_array[i]);
//      Serial.print(" ");
//    }
//    Serial.println();
    
    memcpy(med_array, vm2, sizeof(med_array[0])*MEDIAN_COUNT);
    qsort(med_array, lt_length, sizeof(med_array[0]), sort_desc);
    val2 = med_array[MEDIAN_COUNT/2];
    
    memcpy(med_array, vm3, sizeof(med_array[0])*MEDIAN_COUNT);
    qsort(med_array, lt_length, sizeof(med_array[0]), sort_desc);
    val3 = med_array[MEDIAN_COUNT/2];
    
  } else {
    val0 = v0;
    val1 = v1;
    val2 = v2;
    val3 = v3;
  }

  myservo0.write(map(val0, 0, 1023, 180, 0));
  myservo1.write(map(val1, 0, 1023, 180, 0));
  myservo2.write(map(val2, 0, 1023, 180, 0));
  myservo3.write(map(val3, 0, 1023, 180, 0));
  delay(5);
}

// qsort requires you to create a sort function
int sort_desc(const void *cmp1, const void *cmp2)
{
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
}
