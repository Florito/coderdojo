// https://learn.adafruit.com/adafruit-arduino-lesson-14-servo-motors/if-the-servo-misbehaves

#include <Servo.h>

Servo myservo0;  // create servo object to control a servo
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
Servo myservo3;  // create servo object to control a servo

#define SERIAL_DEBUG false

#define pot0 0
#define pot1 1
#define pot2 2
#define pot3 3

#define SERVO_AUTO_ROTATE false
#define servo0 9
#define servo1 6
#define servo2 5
#define servo3 3

#define T true
#define F false

#define SERVO_RITMO true

int ritmoMax = 64;
int ritmoIndex = 0;
int ritmoDelay = 200;
String ritmo0 = "X..X..X.X..X..X.X..X..X.X..X..X.X..X..X.X..X..X.X..X..X.X.......";
String ritmo1 = "X..X.XXXX..X.....X.XXXXXX....XXXX..X.XXXX..X.....X.XXXXXX....XXX";
int ritmo0Step = 30;
int ritmo1Step = 30;

int v0, v1, v2, v3;
int s0, s1, s2, s3;

void setup() {
  myservo0.attach(servo0);
  myservo1.attach(servo1);
  myservo2.attach(servo2);
  myservo3.attach(servo3);
  if (SERIAL_DEBUG) {
    Serial.begin(115200);
  }
  s0 = s1 = s2 = s3 = 0;
  //  TIMSK0=0; // disable millis(), delay etc
}

void loop() {

  v0 = analogRead(pot0);
  v1 = analogRead(pot1);
  v2 = analogRead(pot2);
  v3 = analogRead(pot3);

  if (SERIAL_DEBUG) {
    Serial.print(v0);
    Serial.print(" ");
    Serial.print(v1);
    Serial.print(" ");
    Serial.print(v2);
    Serial.print(" ");
    Serial.print(v3);
    Serial.print(" ");
  }

  if (!SERVO_RITMO) {
    s0 = map(v0, 0, 1023, 180, 0);
    s1 = map(v1, 0, 1023, 180, 0);
    s2 = map(v2, 0, 1023, 180, 0);
    s3 = map(v3, 0, 1023, 180, 0);
  }

  //  s0 = map(millis(),0,1000,0,180)%180;

  if (SERVO_AUTO_ROTATE) {



    int ms = millis() % 10000;
    if (ms < 5000) {
      s0 = map(ms, 0, 5000, 0, 180);
    } else {
      s0 = map(ms, 5000, 10000, 180, 0);
    }
    s1 = s2 = s3 = s0;
  }

  if (SERVO_RITMO) {

    ritmoDelay = map(v0, 0, 1023, 100, 200);

    char ritmoChar = ritmo0.charAt(ritmoIndex);
    if (ritmoChar == 'X') {
      s0 += ritmo0Step;
      if (ritmo0Step > 0 && s0 > 180) {
        s0 = 180 - ritmo0Step;
        ritmo0Step = -ritmo0Step;
      }
      else if (ritmo0Step < 0 && s0 < 0) {
        s0 = -ritmo0Step;
        ritmo0Step = -ritmo0Step;
      }
    }
    ritmoChar = ritmo1.charAt(ritmoIndex);
    if (ritmoChar == 'X') {
      s1 += ritmo1Step;
      if (ritmo1Step > 1 && s1 > 180) {
        s1 = 180 - ritmo1Step;
        ritmo1Step = -ritmo1Step;
      }
      else if (ritmo1Step < 0 && s1 < 0) {
        s1 = -ritmo1Step;
        ritmo1Step = -ritmo1Step;
      }
    }
    s2 = s1;
    s3 = s0;


    ritmoIndex++;
    ritmoIndex %= ritmoMax;
  }

  if (SERIAL_DEBUG) {
    Serial.print(s0);
    Serial.print(" ");
    Serial.print(s1);
    Serial.print(" ");
    Serial.print(s2);
    Serial.print(" ");
    Serial.print(s3);
    Serial.println();
  }

  myservo0.write(s0);
  myservo1.write(s1);
  myservo2.write(s2);
  myservo3.write(s3);

  delay( SERVO_RITMO ? ritmoDelay : 15);
}

