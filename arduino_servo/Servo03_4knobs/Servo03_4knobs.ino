
#include <Servo.h>

Servo myservo0;  // create servo object to control a servo
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
Servo myservo3;  // create servo object to control a servo

#define pot0 0
#define pot1 1
#define pot2 2
#define pot3 3

#define servo0 9
#define servo1 10
#define servo2 11
#define servo3 12


void setup() {
  myservo0.attach(servo0);
  myservo1.attach(servo1);
  myservo2.attach(servo2);
  myservo3.attach(servo3);
}

void loop() {
  myservo0.write(map(analogRead(pot0), 0, 1023, 180, 0)); 
  myservo1.write(map(analogRead(pot1), 0, 1023, 180, 0)); 
  myservo2.write(map(analogRead(pot2), 0, 1023, 180, 0)); 
  myservo3.write(map(analogRead(pot3), 0, 1023, 180, 0)); 
  delay(15);
}
