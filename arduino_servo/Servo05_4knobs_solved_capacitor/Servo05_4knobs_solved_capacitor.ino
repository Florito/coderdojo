// https://learn.adafruit.com/adafruit-arduino-lesson-14-servo-motors/if-the-servo-misbehaves

#include <Servo.h>

Servo myservo0;  // create servo object to control a servo
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
Servo myservo3;  // create servo object to control a servo

#define SERIAL_DEBUG false

#define pot0 0
#define pot1 1
#define pot2 2
#define pot3 3

#define SERVO_AUTO_ROTATE false
#define servo0 9
#define servo1 6
#define servo2 5
#define servo3 3

int v0, v1, v2, v3;
int s0, s1, s2, s3;

void setup() {
  myservo0.attach(servo0);
  myservo1.attach(servo1);
  myservo2.attach(servo2);
  myservo3.attach(servo3);
  if (SERIAL_DEBUG) {
    Serial.begin(115200);
  }
  //  TIMSK0=0; // disable millis(), delay etc
}

void loop() {

  v0 = analogRead(pot0);
  v1 = analogRead(pot1);
  v2 = analogRead(pot2);
  v3 = analogRead(pot3);

  if (SERIAL_DEBUG) {
    Serial.print(v0);
    Serial.print(" ");
    Serial.print(v1);
    Serial.print(" ");
    Serial.print(v2);
    Serial.print(" ");
    Serial.print(v3);
    Serial.print(" ");
  }

  s0 = map(v0, 0, 1023, 180, 0);
  s1 = map(v1, 0, 1023, 180, 0);
  s2 = map(v2, 0, 1023, 180, 0);
  s3 = map(v3, 0, 1023, 180, 0);

  //  s0 = map(millis(),0,1000,0,180)%180;

  if (SERVO_AUTO_ROTATE) {
    int ms = millis() % 10000;
    if (ms < 5000) {
      s0 = map(ms, 0, 5000, 0, 180);
    } else {
      s0 = map(ms, 5000, 10000, 180, 0);
    }
    s1 = s2 = s3 = s0;
  }

  if (SERIAL_DEBUG) {
    Serial.print(s0);
    Serial.print(" ");
    Serial.print(s1);
    Serial.print(" ");
    Serial.print(s2);
    Serial.print(" ");
    Serial.print(s3);
    Serial.println();
  }

  myservo0.write(s0);
  myservo1.write(s1);
  myservo2.write(s2);
  myservo3.write(s3);
  delay(15);
}

